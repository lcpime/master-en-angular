import { Component } from '@angular/core';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
})
export class LibrosComponent {
  libros = ['Libro 1', 'Libro 2', 'Libro 3'];


  eliminarLibro(libro: string){
    this.libros = this.libros.filter(p => p !== libro);
  }

  guardarLibro(f){
    if(f.valid){
      this.libros.push(f.value.nombreLibro);
    }
  }
}
